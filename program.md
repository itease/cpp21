# Программа курса по С++ 2021-2022 (still in dev)


## Введение


### 1. Введение

1. Информация про курс
2. Общая информация про язык
3. Code style
3. Hello world

### 2. Немного про память

1. new и delete


### 3. Функции и указатели на них

1. Аргументы по умолчанию
2. Перегрузка функций
3. Указатели на функцию
4. Функции с переменным числом аргументов

### 4. Модификаторы типов. Ссылки и константы

1. Модификаторы типов
    1. signed, unsigned, long, short
    2. volatile
    3. extern
2. Константы
3. Ссылки

### 5. Приведение типов

1. static_cast
2. reinterpret_cast
3. const_cast
4. c-style cast
5. dynamic_cast 


## ООП


### 1. Введение в ООП

1. Основные термины
2. Структуры в С
3. Классы и структуры в С++
    1. Поля и методы
    2. private, public, protected
4. Конструкторы и деструкторы
    1. Конструктор
    2. Конструктор копирования
    3. Делегирующие конструкторы
    3. Деструктор
4. Списки инициализации
6. Ключевое слово explicit
7. Оператор присваивания, copy and swap
8. Правило трех (пяти)
7. Операторы ".", "->" и this
8. Константные методы
9. Ключевое слово mutable
10. Ключевое слово friend
11. Ключевое слово static
12. Указатели на члены


### 2. Перегрузка операторов

1. Арифметические операторы
2. Операторы сравнения
3. Инкремент и декремент
4. Круглые скобки
5. Операторы "&&", "||" и ","
6. Операторы "*", "&" и "-﹥", "->*"
7. Перегрузка C-style cast

### 3. Наследование

1. Основная идея
2. Public, protected, private наследование
4. Поиск имен при наследовании
    1. Сокрытие имен при наследовании 
    2. Явное обращение к предку
    3. friend при наследовании
5. Порядок вызова конструкторов и деструкторов
6. Приведение типов при наследовании
7. Множественное наследование
    1. Основная идея
    1. Проблема ромбовидного наследования
8. Виртуальное наследование
9. Полиморфизм
10. Виртуальные функции
    1. Что это и зачем
    2. Поиск имен
11. Виртуальный деструктор
12. Слова override и final
12. Абстрактные классы и pure virtual функции
13. typeid
14. RTTI и dynamic_cast
15. vtables


### 4. Шаблоны

1. Основная идея
2. Объявление шаблонов
3. Перегрузка шаблонных функций
4. Специализация шаблонов
5. Non-type template parameters 
5. typedef
6. Проблема зависимых имен
7. type traits
8. Инстанцирование 
8. Variadic templates
9. Fold expressions
10. Правила вывода шаблонных типов